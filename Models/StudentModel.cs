﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class StudentModel
    {
        public Student student { get; set; }
        public Department department { get; set; }
        public ClassRoom classRoom { get; set; }
    }

    public class StudentPointModel
    {
        public Student student { get; set; }
        public Point points { get; set; }
        public Cours course { get; set; }



    }
}