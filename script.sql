USE [ProjectWeb]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 4/27/2022 8:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[DOB] [nvarchar](50) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClassRoom]    Script Date: 4/27/2022 8:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassRoom](
	[ClassId] [int] NOT NULL,
	[ClassName] [nvarchar](50) NOT NULL,
	[DepartmentId] [int] NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Courses]    Script Date: 4/27/2022 8:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[CourseraName] [nvarchar](50) NULL,
	[CourseraKey] [nvarchar](50) NULL,
 CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Departments]    Script Date: 4/27/2022 8:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departments](
	[DepartmentId] [int] NOT NULL,
	[DepartmentName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Points]    Script Date: 4/27/2022 8:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Points](
	[StudentId] [int] NULL,
	[CourseId] [int] NULL,
	[Fifteen_minutes] [float] NULL,
	[Fortyfive_minutes] [float] NULL,
	[Semester] [float] NULL,
	[pointsId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Points] PRIMARY KEY CLUSTERED 
(
	[pointsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Student]    Script Date: 4/27/2022 8:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[StudentId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Sex] [bit] NOT NULL,
	[DOB] [date] NOT NULL,
	[ClassId] [int] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (1, N'Le Van Teo', N'1999-9-9', N'teo@gmail.com', N'123')
INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (2, N'Nguyen Van Trung', N'2000-2-2', N'trung@gmail.com', N'123')
INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (3, N'Le Van Tung', N'1999-11-11', N'levantung@gmail.com', N'123')
INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (14, N'test1', N'1999-12-31', N'test1@gmail.com', N'123')
INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (15, N'test2', N'1999-11-11', N'test2@gmail.com', N'123')
INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (16, N'test10', N'1999-11-11', N'test10@gmail.com', N'123')
INSERT [dbo].[Account] ([Id], [FullName], [DOB], [Username], [Password]) VALUES (17, N'test12', N'1999-11-11', N'test12@gmail.com', N'123')
SET IDENTITY_INSERT [dbo].[Account] OFF
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (1, N'IA01', 1)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (2, N'IA02', 1)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (3, N'IA03', 1)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (4, N'SE01', 2)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (5, N'SE02', 2)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (6, N'SE03', 2)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (7, N'BM01', 3)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (8, N'MK02', 3)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (9, N'BM03', 3)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (10, N'QT01', 4)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (11, N'QT02', 4)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (12, N'QT03', 4)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (13, N'TT01', 5)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (14, N'TT02', 5)
INSERT [dbo].[ClassRoom] ([ClassId], [ClassName], [DepartmentId]) VALUES (15, N'TT03', 5)
SET IDENTITY_INSERT [dbo].[Courses] ON 

INSERT [dbo].[Courses] ([CourseId], [CourseraName], [CourseraKey]) VALUES (1, N'Mathematics for engineering', N'MAE101')
INSERT [dbo].[Courses] ([CourseId], [CourseraName], [CourseraKey]) VALUES (2, N'Computer networking', N'NWC203c')
INSERT [dbo].[Courses] ([CourseId], [CourseraName], [CourseraKey]) VALUES (3, N'Programming Fundamentals', N'PRF192')
SET IDENTITY_INSERT [dbo].[Courses] OFF
INSERT [dbo].[Departments] ([DepartmentId], [DepartmentName]) VALUES (1, N'Trí tuệ nhân tạo')
INSERT [dbo].[Departments] ([DepartmentId], [DepartmentName]) VALUES (2, N'Kỹ thuật phần mềm')
INSERT [dbo].[Departments] ([DepartmentId], [DepartmentName]) VALUES (3, N'Bảo mật thông tin')
INSERT [dbo].[Departments] ([DepartmentId], [DepartmentName]) VALUES (4, N'Quản trị khách sạn')
INSERT [dbo].[Departments] ([DepartmentId], [DepartmentName]) VALUES (5, N'Truyền thông đa phương tiện')
SET IDENTITY_INSERT [dbo].[Points] ON 

INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (3, 3, 8, 8, 6.5, 3)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (4, 3, 2, 2, 3, 4)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (5, 2, 7, 8, 6, 5)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (6, 1, 6.5, 6.5, 6.5, 6)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (7, 2, 8, 9, 7, 7)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (8, 1, 7, 7, 2, 8)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (9, 3, 5, 7, 8, 9)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (2, 2, 4.5, 6, 4, 10)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (1, 1, 8, 8, 8, 14)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (1, 1, 8, 8, 8, 15)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (1, 1, 9, 9, 9, 17)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (3, 3, 8, 9, 7, 30)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (1, 1, 5, 6, 7, 42)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (1, 1, 8, 9, 7, 43)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (3, 3, 7, 8, 9, 44)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (3, 3, 9, 8, 7, 46)
INSERT [dbo].[Points] ([StudentId], [CourseId], [Fifteen_minutes], [Fortyfive_minutes], [Semester], [pointsId]) VALUES (2, 2, 8, 7, 6, 51)
SET IDENTITY_INSERT [dbo].[Points] OFF
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (1, N'Lưu Đức Anh', 1, CAST(N'2001-02-24' AS Date), 1)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (2, N'Lưu Đức Cảnh', 1, CAST(N'2008-08-03' AS Date), 1)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (3, N'Đoàn Trọng Bách', 1, CAST(N'2003-11-11' AS Date), 2)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (4, N'Đinh Duy Hoàn', 0, CAST(N'2005-03-21' AS Date), 3)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (5, N'Đặng Văn Hậu', 1, CAST(N'2006-04-24' AS Date), 4)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (6, N'Nguyễn Công KIên', 1, CAST(N'2004-08-29' AS Date), 5)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (7, N'Nguyễn Đức Chính', 1, CAST(N'2001-11-22' AS Date), 3)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (8, N'Đinh Minh Thu', 0, CAST(N'2002-12-24' AS Date), 5)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (9, N'Nguyễn Ái Thi', 0, CAST(N'2003-02-05' AS Date), 2)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (10, N'Phạm Văn Mạnh', 1, CAST(N'2001-07-24' AS Date), 4)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (11, N'Đào Ngọc Huyền', 0, CAST(N'2001-02-07' AS Date), 4)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (12, N'Hoàng Quỳnh Anh', 0, CAST(N'2001-09-24' AS Date), 1)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (13, N'Cao Thái Sơn', 1, CAST(N'2005-10-24' AS Date), 5)
INSERT [dbo].[Student] ([StudentId], [Name], [Sex], [DOB], [ClassId]) VALUES (14, N'Lương Trung Kiên', 1, CAST(N'2001-02-16' AS Date), 2)
ALTER TABLE [dbo].[ClassRoom]  WITH CHECK ADD  CONSTRAINT [FK_Class_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[ClassRoom] CHECK CONSTRAINT [FK_Class_Departments]
GO
ALTER TABLE [dbo].[Points]  WITH CHECK ADD  CONSTRAINT [FK_Points_Courses] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[Points] CHECK CONSTRAINT [FK_Points_Courses]
GO
ALTER TABLE [dbo].[Points]  WITH CHECK ADD  CONSTRAINT [FK_Points_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([StudentId])
GO
ALTER TABLE [dbo].[Points] CHECK CONSTRAINT [FK_Points_Student]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_Student_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[ClassRoom] ([ClassId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_Student_Class]
GO
