﻿using project.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace project.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student/ListStudents
        public ActionResult Create(int id)
        {
            TempData["IDSTU"] = id;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Point point)
        {
            using (var context = new ProjectWebEntities3())
            {
                var id = TempData["IDSTU"];
                point.StudentId = (int?)id;
                point.CourseId = (int?)id;
                context.Points.Add(point);
                context.SaveChanges(); 
            }
            ViewBag.Message = "Created pointed successful";
            return View();
        }

        public ActionResult ListStudents()
        {
            using (var context = new ProjectWebEntities3())
            {
                List<Student> students = context.Students.ToList();
                List<Department> departments = context.Departments.ToList();
                List<ClassRoom> classRooms = context.ClassRooms.ToList();
                
                 var studentsDetails = from s in students
                                      join c in classRooms on s.ClassId equals c.ClassId into table1
                                      from c in table1.ToList()
                                      join i in departments on c.DepartmentId equals i.DepartmentId into table2
                                      from i in table2.ToList()
                                      select new StudentModel
                                      {
                                          student = s,
                                          department = i,
                                          classRoom = c
                                      };
                return View(studentsDetails);
            }
        }

        // GET: Student/Details/5
        public ActionResult Details(int id)
        {
            using (var context = new ProjectWebEntities3())
            {
                List<Student> students = context.Students.ToList();
                List<Point> points = context.Points.ToList();
                List<Cours> cours = context.Courses.ToList();
                var pointDetails = from s in students  
                                      join p in points on s.StudentId equals p.StudentId into table1
                                      from p in table1.ToList()
                                      join c in cours on p.CourseId equals c.CourseId into table2
                                      from c in table2.ToList()
                                      where s.StudentId == id
                                      select new StudentPointModel
                                      {
                                          student = s,
                                          points = p,
                                          course = c
                                      };
                TempData["ID"] = id;
                return View("Details",pointDetails);
            }
        }


        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            using (var context = new ProjectWebEntities3())
            {
                var data = context.Students.Include("ClassRoom").Where(x => x.StudentId == id).FirstOrDefault();
                return View(data);
            }
        }

        // POST: Student/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (var context = new ProjectWebEntities3())
                {
                    Student students = context.Students.Where(x => x.StudentId == id).FirstOrDefault();
                    context.Students.Remove(students);
                    context.SaveChanges();
                    TempData["SMS_DELETE_STUDENT"] = "Delete Student successfully";
                }

                return RedirectToAction("ListStudents");
            }
            catch
            {
                return View();
            }
        }

        // GET: Point/Edit/5
        public ActionResult Edit(int id)
        {
            using (var context = new ProjectWebEntities3())
            {
                var data = context.Points.Include("Student").Where(x => x.pointsId == id).FirstOrDefault();
                return View(data);
            }
        }

        // POST: Point/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Point point)
        {
            System.Diagnostics.Debug.WriteLine("Save Called");
            try
            {
                    // TODO: Add update logic here
                    using (var context = new ProjectWebEntities3())
                    {
                        context.Entry(point).State = EntityState.Modified;
                        context.SaveChanges();
                        TempData["SMS_EDIT_POINT"] = "Edit Point successfully";
                        var link = "Details\\"+id;
                        return RedirectToAction(link);
                    }
                }
            catch
            {
                return View();
            }
        }

        // GET: Point/Delete/5
        public ActionResult DeletePoint(int id)
        {
            using (var context = new ProjectWebEntities3())
            {
                var data = context.Points.Where(x => x.pointsId == id).FirstOrDefault();
                return View(data);
            }
        }

        // POST: Point/Delete/5
        [HttpPost]
        public ActionResult DeletePoint(int id, Point collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (var context = new ProjectWebEntities3())
                {
                    Point points = context.Points.Where(x => x.pointsId == id).FirstOrDefault();
                    context.Points.Remove(points);
                    context.SaveChanges();
                }
                TempData["SMS_DELETE_POINT"] = "Delete Point successfully";
                return RedirectToAction("ListStudents");
            }
            catch
            {
                return View();
            }
        }
    }
}
