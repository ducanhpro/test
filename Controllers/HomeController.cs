﻿using project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace project.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /*public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }*/

        public ActionResult Login()
        {
            return View();
        }

        // login page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Account objUser)
        {
            if (ModelState.IsValid)
            {
                using (var context = new ProjectWebEntities3())
                {
                    var obj = context.Accounts.Where(a => a.Username.Equals(objUser.Username) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["Username"] = obj.Username.FirstOrDefault().ToString();
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(objUser);
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();//remove session
            return RedirectToAction("Login");
        }

        // view registration
        public ActionResult Register()
        {
            return View();
        }

        // handler registration
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Account objUser)
        {
            if (ModelState.IsValid)
            {
                // To open a connection to the database
                using (var context = new ProjectWebEntities3())
                {
                    Account acclog = new Account();
                    var check = context.Accounts.FirstOrDefault(e => e.Username == objUser.Username);
                    if (check == null)
                    {
                        acclog.FullName = objUser.FullName;
                        acclog.Username = objUser.Username;
                        acclog.Password = objUser.Password;
                        acclog.DOB = objUser.DOB;

                        // Add data to the particular table
                        context.Accounts.Add(objUser);

                        // save the changes
                        context.SaveChanges();

                        // To display the message on the screen

                        TempData["SMS_REGISTER"] = "Created the Account successfully";
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        ViewBag.error = "Email already exists";
                        return View("Register");
                    }
                }
               
            }
            else
            {
                return View("Register", objUser);
            }
        }
    }
}